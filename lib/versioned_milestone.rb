# frozen_string_literal: true

VersionedMilestone = Struct.new(:context) do
  def beyond_n_plus_1_milestone?
    all.first(2).map(&:title).none?(context.milestone.title)
  end

  def current
    all.first
  end

  def all
    @all ||= all_active.select do |m|
      m.title.match?(/\A\d+\.\d+\z/) # So we don't pick 2019
    end
  end

  private

  def root_milestone
    @root_milestone ||= Gitlab::Triage::Resource::Milestone.new(
      { group_id: context.root_id },
      parent: context,
      redact_confidentials: false
    )
  end

  def all_active
    @all_active ||= root_milestone.__send__(:all_active_with_start_date)
  end
end
